const state_stack = require('../util/state.js');
const db = require('../util/db.js');
const { MessageCollector } = require('discord.js');

module.exports = (bot, member) => {
    state_stack.push(member.guild.id);

    const channel = member.guild.channels.find(ch => ch.name === 'role-assignment');
    channel.send(`> Hello, ${member}! My job as the @claw bot#6894 is to set your nickname and roles so you can access the rest of the server! Please post a message in the format Name｜12345A to indicate your name and team affiliation.`);

    const check = member.guild.roles.find(r => r.name === '✓');

    db.ref(`${member.guild.id}/regex`).once('value')
        .then(val => {
            val = val.exportVal();
            const collector = new MessageCollector(channel, msg => msg.member.user.username === member.user.username && msg.content.match(new RegExp(val.regex, "g")), {
                max: 1
            });
            collector.on('collect', (msg, c) => {
                c.stop();

                const m = msg.toString();
                const array = m.split(new RegExp(val.regex)).filter(i => i.length > 0);
                const nick = array[0];
                const requested_roles = array.slice(1, array.length);

                db.ref(`${member.guild.id}/roles`).once('value')
                    .then(val => {
                        val = val.exportVal();
                        const regex = Object.values(val).map(i => ({
                            regex: i.regex,
                            id: i.id
                        }));
                        const resolved_roles = regex.filter(re => requested_roles.find(r => r.match(new RegExp(re.regex, "gi"))));

                        if (resolved_roles.length !== requested_roles.length) throw Error('Missing some or all requested roles!');
                        const mapped_roles = resolved_roles.map(role => role.id);

                        member.addRoles([check, ...mapped_roles])
                            .then(() => {
                                member.setNickname([nick, ...requested_roles].join('｜'));
                                msg.reply('I have successfully added your roles! You are now granted access to the rest of the server.');
                                state_stack.pop(member.guild.id);
                            })
                            .catch(e => {
                                console.log(e);
                                msg.reply(`Unfortunately I, your humble @claw bot#6894, do not possess the intelligent simplicity required to process your request. An admin will manually add your roles shortly.\n**Reason: **${e.message}`);
                                member.addRole(check);
                                state_stack.pop(member.guild.id);
                            });
                    })
                    .catch(e => {
                        console.log(e);
                        msg.reply(`Unfortunately I, your humble @claw bot#6894, do not possess the intelligent simplicity required to process your request. An admin will manually add your roles shortly.\n**Reason: **${e.message}`);
                        member.addRole(check);
                        state_stack.pop(member.guild.id);
                    });
            });
        })
        .catch(e => {
            console.log(e);
            channel.send(`Unfortunately I, your humble @claw bot#6894, do not possess the intelligent simplicity required to process your request. An admin will manually add your roles shortly.\n**Reason: **${e.message}`);
            member.addRole(check);
            state_stack.pop(member.guild.id);
        });
}