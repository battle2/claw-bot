const state_stack = require('../util/state.js');

module.exports = (bot, msg) => {
    if (msg.author.bot) return;

    if (msg.content.startsWith(process.env.PREFIX)) {
        if (state_stack.has(msg.guild.id)) {
            msg.reply('I am currently handling another process, please try again later!');
            return;
        }
        try {
            const args = msg.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
            const command = args.shift();
            args.push(msg);

            if (!(msg.member.hasPermission(bot.cmds[command].permission_level) || msg.member.id === '423699849767288853')) {
                msg.reply('you do not have permission to run that command!');
                return;
            }

            state_stack.push(msg.guild.id);

            bot.cmds[command].action(args);
        } catch (e) {
            msg.reply('there is no such command!');
            console.log(e);
            state_stack.pop(msg.guild.id);
        }
    }
}