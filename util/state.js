module.exports = {
  stack: [],
  ready: false,
  has(server) {
    return this.stack.includes(server);
  },
  push(server) {
    this.stack.push(server);
  }, 
  pop(server) {
    this.stack.splice(this.stack.indexOf(server), 1);
  },
  log() {
    console.log(this.stack);
  }
}