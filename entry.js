const { Client } = require('discord.js');
const fs = require('fs');
require('dotenv').config();
const state_stack = require('./util/state.js');

const bot = new Client();
bot.cmds = {};

async function load() {
    fs.readdir('./commands', (err, files) => {
        if (err) return console.error(err);
        files.forEach(file => {
            if (!file.endsWith(".js")) return;
            const name = file.split('.js')[0];
            bot.cmds[name] = require(`./commands/${name}`);
        });
        console.log('Commands Initialized');
    });

    fs.readdir('./events', (err, files) => {
        if (err) return console.error(err);
        files.forEach(file => {
            if (!file.endsWith(".js")) return;
            const event = require(`./events/${file}`);
            let eventName = file.split(".")[0];
            bot.on(eventName, event.bind(null, bot));
            delete require.cache[require.resolve(`./events/${file}`)];
        });
    });
}

bot.on('ready', () => {
    bot.user.setStatus('online');
    bot.user.setPresence({
        game: {
            name: `for new members in ${bot.guilds.array().length} servers`,
            type: "WATCHING"
        }
    });
    load();
});

bot.login(process.env.TOKEN);