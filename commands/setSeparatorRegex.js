const db = require('../util/db.js');
const state_stack = require('../util/state.js');
const { Permissions } = require('discord.js');

module.exports = {
    action(args) {
        const msg = args[args.length - 1];
        db.ref(`${msg.guild.id}/regex`).set({
            regex: args[0]
        })
            .then(() => {
                msg.reply('successfully updated the separator regex!');
                state_stack.pop(msg.guild.id);
            })
            .catch(e => {
                msg.reply('an error occured!');
                state_stack.pop(msg.guild.id);
            })
    },
    permission_level: Permissions.FLAGS.MANAGE_GUILD
}