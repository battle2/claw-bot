const db = require('../util/db.js');
const state_stack = require('../util/state.js');
const { RichEmbed, MessageCollector, Permissions } = require("discord.js");

module.exports = {
    action(args) {
        const msg = args[args.length - 1];
        msg.channel.send('[1/2] Please provide a regex.');
        const regex_collector = new MessageCollector(msg.channel, m => m.content.match(/\/(.+)\//) && !m.author.bot, { max: 1 });
        regex_collector.on('collect', (regex, c) => {
            regex = regex.toString().substring(1, regex.toString().length - 1);
            c.stop();
            msg.channel.send('[2/2] Please provide a role ID for this regex.');
            const role_collector = new MessageCollector(msg.channel, m => msg.guild.roles.find(r => r.id === m.content), { max: 1 });
            role_collector.on('collect', (id, c) => {

                id = id.toString();
                const role = msg.guild.roles.find(r => r.id === id);
                c.stop();
                const emb = new RichEmbed();
                emb.setTitle('Does this look correct?')
                    .addField('RegExp', regex)
                    .addField('Role', role.name)
                    .addField('ID', id)
                    .setColor(0xFFD700);

                msg.channel.send(emb);
                const confirmation = new MessageCollector(msg.channel, m => (m.content.toLowerCase().startsWith('y') || m.content.toLowerCase().startsWith('n')) && !m.author.bot, { max: 1 });
                confirmation.on('collect', (answer, c) => {
                    answer = answer.toString().toLowerCase();
                    c.stop();
                    if (answer.startsWith('y')) {
                        db.ref(`${msg.guild.id}/roles/${role.name}`).set({ regex, ts: Date.now(), id })
                            .then(() => {
                                msg.channel.send('Entry addition successful! Exiting command now.');
                                state_stack.pop(msg.guild.id);
                            })
                            .catch(e => {
                                msg.channel.send('Entry addition failed! Exiting command now.');
                                state_stack.pop(msg.guild.id);
                            });

                        return;
                    }
                    else {
                        msg.channel.send('Entry addition aborted, exiting command now.');
                        state_stack.pop(msg.guild.id);
                        return;
                    }
                });
            });
        });
    },
    permission_level: Permissions.FLAGS.MANAGE_GUILD
};