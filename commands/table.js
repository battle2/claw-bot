const db = require('../util/db.js');
const state_stack = require('../util/state.js');
const { RichEmbed, Permissions } = require("discord.js");

module.exports = {
    action(args) {
        const msg = args[args.length - 1];
        const emb = new RichEmbed();

        emb.setTitle('Lookup Table');
        db.ref(`${msg.guild.id}/roles`).once('value')
            .then(val => {
                val = val.exportVal();

                for (let i = 0; i < Object.keys(val).length; i++) {
                    emb.addField(`${i} (${Object.keys(val)[i]})`, `**ID: **${val[Object.keys(val)[i]].id}\n**Regex: ** ${val[Object.keys(val)[i]].regex}`, true);
                }

                msg.channel.send(emb);
                state_stack.pop(msg.guild.id);
            })
            .catch(e => {
                msg.reply('there is no data in the table!');
                state_stack.pop(msg.guild.id);
            })
    },
    permission_level: Permissions.FLAGS.MANAGE_GUILD
}