const state_stack = require('../util/state.js');
const { Permissions } = require('discord.js');

module.exports = {
    action(args) {
        const msg = args[args.length - 1];
        msg.guild.createRole({
            name: args[0],
            color: args[1]
        })
            .then(() => {
                msg.reply('New role created!');
                state_stack.pop(msg.guild.id);
            })
            .catch(e => {
                console.log(e);
                msg.reply('An error occured!');
                state_stack.pop(msg.guild.id);
            });
    },
    permission_level: Permissions.FLAGS.MANAGE_GUILD
}