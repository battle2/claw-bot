const db = require('../util/db.js');
const state_stack = require('../util/state.js');
const { Permissions } = require('discord.js');

module.exports = {
    action(args) {
        const msg = args[args.length - 1];
        db.ref(`${msg.guild.id}/roles`).once('value')
            .then(data => {

                const val = data.exportVal();
                const array = [];

                for (let i = 0; i < Object.keys(val).length; i++) {
                    array.push(Object.keys(val)[i]);
                }

                if (array[args[0]] === undefined) {
                    msg.reply('no data at specified cell!');
                    state_stack.pop(msg.guild.id);
                    return;
                }

                db.ref(`${msg.guild.id}/roles/${array[args[0]]}`).remove()
                    .then(() => {
                        msg.reply(`successfully removed data at cell ${args[0]}!`);
                        state_stack.pop(msg.guild.id);
                    })
                    .catch(e => {
                        msg.reply('an error occured!');
                        console.log(e);
                        state_stack.pop(msg.guild.id);
                    })
            })
            .catch(e => {
                msg.reply(`could not remove data at cell ${args[0]}!`);
                console.log(e);
                state_stack.pop(msg.guild.id);
            })
    },
    permission_level: Permissions.FLAGS.MANAGE_GUILD
}