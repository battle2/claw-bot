# claw bot

`claw bot` is a Discord bot that helps VEX Robotics servers with roles/verification. It is written using discord.js

## Configuration

1. Invite `claw bot` to your server using this [link](https://discordapp.com/api/oauth2/authorize?client_id=642872037970542604&permissions=402738176&scope=bot). The prefix is `]`.
2. Create a channel named `role-assignment`. This is where `claw bot` is active and will watch for new members.
3. Configure the separator regular expression. This will determine what claw bot should use to separate names and roles (e.g., with "Battlesquid | 2612A", "|" is the separator). **`claw bot` will not work if you do not initialize this setting!**
4. Configure the regular expressions `claw bot` should look for using **addEntry**. `claw bot` will use these regular expressions to grant roles to new users.


## Commands (Admnistrators Only)

**addEntry**: `claw bot` asks for a regular expression and a role ID. After confirmation (yes or no), `claw bot` will push your entry to the database.

**deleteEntry [`number`]**: `claw bot` will delete the entry at the specified index.

**table**: `claw bot` displays a table of your roles configured via **addEntry**. From here, you will be able to see the index of each role (useful for **deleteEntry**) as well as the regular expression and role ID that it applies.

**setSeparatorRegex**: sets the separator regex that `claw bot` will use to separate names and roles. If you do not know what to do, use `]setSeparatorRegex [|\s]`.